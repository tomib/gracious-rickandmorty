# gracious | Rick & Morty

Demo: http://www.tomislavbisof.com/gracious-rickandmorty

Project template based on react-init-sc: https://github.com/tomislavbisof/react-init-sc

Test assignment for gracious.nl using the rickandmorty.com GraphQL API.

Note that the API can be buggy and in some cases return strings or nulls instead of iterables, and also has a lot of missing data, but the project has been made to work around that.

I did not have time to write indepth tests, but every component has one to check it renders properly.

## Includes

- [Styled Components](https://www.styled-components.com/)
- [ESLint](https://eslint.org)
- [Airbnb Style Guide](https://github.com/airbnb/javascript)
- [Webpack 4](https://webpack.js.org)
- [React Router](https://reacttraining.com/react-router)
- [PropTypes](https://github.com/facebook/prop-types)
- [Enzyme](http://airbnb.io/enzyme/)
- [Jest](https://facebook.github.io/jest/)

### Installation

To install the template, simply clone or download the repository, navigate to the project root and run:

```
npm install
```

### Usage

For development with the Webpack Dev Server and HMR, use:

```
npm run watch
```

To build the project for production, use:

```
npm run build
```

Your project files will be located in the `/dist/` folder after building. Images smaller than 8KB will be embedded, while larger ones will be located in `/dist/assets/`.

To run tests, use:

```
npm run test
```

### License

This started out as a quick-start template for my own projects, so feel free to modify or use it as you like.
