import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { createBrowserHistory } from 'history';

import App from './components/app/App';

const client = new ApolloClient({
  uri: 'https://rickandmortyapi.com/graphql/',
});

ReactDOM.render(
  <Router history={createBrowserHistory()}>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </Router>,
  document.getElementById('root'),
);
