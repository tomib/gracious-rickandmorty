import React from 'react';
import { shallow } from 'enzyme';
import Character from './Character';

describe('tests the Character component', () => {
  const wrapper = shallow(<Character match={{ params: { id: 1 } }} />);

  it('renders the Character component', () => {
    expect(wrapper.find('[data-test="Character"]').exists()).toBe(true);
  });
});
