import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

import Loading from '../shared/loading/Loading';
import PageHeader from '../shared/pageHeader/PageHeader';
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCell,
} from '../shared/table/Table.styled';
import {
  CharacterStyled,
  Information,
  Image,
  Name,
  Bio,
  Origin,
  Location,
  Appears,
} from './Character.styled';

export default function Character(props) {
  return props.match.params.id ? (
    <CharacterStyled data-test="Character">
      <Query
        query={gql`
          query Character($id: ID) {
            character(id: $id) {
              name
              status
              species
              type
              gender
              origin {
                id
                name
              }
              location {
                id
                name
              }
              image
              episode {
                id
                episode
                name
                air_date
                characters {
                  id
                }
              }
            }
          }
        `}
        variables={{
          id: props.match.params.id,
        }}
        fetchPolicy="cache-and-network"
        // Buggy API, often returns strings instead of iterables.
        errorPolicy="ignore"
      >
        {({ loading, error, data: { character } }) => {
          if (error) {
            return <p>Error :(</p>;
          }

          return (
            <Fragment>
              <PageHeader title="Character Details" />
              {loading ? (
                <Loading text="Loading character..." />
              ) : (
                <Fragment>
                  <Information>
                    <Image src={character.image} alt={character.name} />
                    <Name>
                      {character.name} ({character.status})
                    </Name>
                    <Bio>
                      {character.species}, {character.gender}
                    </Bio>
                    <Origin>
                      Origin:{' '}
                      <Link
                        key={`origin-${character.origin.id}`}
                        to={`/location/${character.origin.id}`}
                      >
                        {character.origin.name}
                      </Link>
                    </Origin>
                    <Location>
                      Location:{' '}
                      <Link
                        key={`location-${character.location.id}`}
                        to={`/location/${character.location.id}`}
                      >
                        {character.location.name}
                      </Link>
                    </Location>
                  </Information>
                  {character.episode &&
                  Array.isArray(character.episode) &&
                  character.episode.length > 0 ? (
                    <Fragment>
                      <Appears>Appears in these episodes:</Appears>
                      <Table>
                        <TableHeader>
                          <TableRow>
                            <TableCell width="15%" textAlign="left">
                              Episode
                            </TableCell>
                            <TableCell width="30%" textAlign="left">
                              Name
                            </TableCell>
                            <TableCell>Aired On</TableCell>
                            <TableCell width="15%" mobileHide>
                              Characters
                            </TableCell>
                          </TableRow>
                        </TableHeader>
                        <TableBody>
                          {character.episode &&
                            Array.isArray(character.episode) &&
                            character.episode.map(
                              ({ id, episode, name, air_date, characters }) => (
                                <TableRow key={episode}>
                                  <TableCell width="10%" textAlign="left">
                                    {episode}
                                  </TableCell>
                                  <TableCell width="30%" textAlign="left">
                                    <Link to={`/episode/${id}`}>{name}</Link>
                                  </TableCell>
                                  <TableCell>{air_date}</TableCell>
                                  <TableCell width="15%" mobileHide>
                                    {characters.length}
                                  </TableCell>
                                </TableRow>
                              ),
                            )}
                        </TableBody>
                      </Table>
                    </Fragment>
                  ) : (
                    <Appears>Does not appear in any known episodes.</Appears>
                  )}
                </Fragment>
              )}
            </Fragment>
          );
        }}
      </Query>
    </CharacterStyled>
  ) : (
    <div>No character selected.</div>
  );
}

Character.propTypes = {
  match: PropTypes.object.isRequired,
};
