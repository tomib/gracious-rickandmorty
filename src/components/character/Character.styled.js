import styled from 'styled-components';

export const CharacterStyled = styled.div`
  padding-bottom: 20px;
`;

export const Information = styled.div`
  margin-bottom: 30px;

  text-align: center;
`;

export const Image = styled.img`
  margin-bottom: 8px;

  border-radius: 4px;

  box-shadow: 0px 2px 8px 0px rgba(0, 0, 0, 0.25);
`;

export const Name = styled.h2`
  font-size: 32px;

  @media (max-width: 460px) {
    font-size: 20px;
  }
`;

export const Bio = styled.div`
  margin-bottom: 8px;

  font-size: 22px;

  @media (max-width: 460px) {
    font-size: 16px;
  }
`;

export const Origin = styled.div`
  margin-bottom: 4px;
`;

export const Location = styled.div``;

export const Appears = styled.div`
  margin-bottom: 14px;

  text-align: center;
`;
