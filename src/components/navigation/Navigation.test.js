import React from 'react';
import { shallow } from 'enzyme';
import Navigation from './Navigation';

describe('tests the Navigation component', () => {
  const wrapper = shallow(<Navigation />);

  it('renders the Navigation component', () => {
    expect(wrapper.find('[data-test="Navigation"]').exists()).toBe(true);
  });
});
