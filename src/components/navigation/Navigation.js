import React from 'react';
import { NavLink } from 'react-router-dom';

import {
  NavigationStyled,
  Content,
  Title,
  Logo,
  Address,
  Menu,
  List,
  Item,
} from './Navigation.styled';

import LogoGracious from '../../assets/gracious.svg';

export default function Navigation() {
  return (
    <NavigationStyled data-test="Navigation">
      <Content>
        <Title>
          <Logo src={LogoGracious} alt="gracious" />
          <Address href="https://rickandmortyapi.com" target="_blank">
            + rickandmortyapi.com
          </Address>
        </Title>
        <Menu>
          <List>
            <Item>
              <NavLink to="/episodes" activeClassName="active">
                episodes
              </NavLink>
            </Item>
            <Item>
              <NavLink to="/characters" activeClassName="active">
                characters
              </NavLink>
            </Item>
            <Item>
              <NavLink to="/locations" activeClassName="active">
                locations
              </NavLink>
            </Item>
          </List>
        </Menu>
      </Content>
    </NavigationStyled>
  );
}
