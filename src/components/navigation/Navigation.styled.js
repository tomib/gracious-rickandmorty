import styled from 'styled-components';
import colors from '../../constants/colors';

export const NavigationStyled = styled.div`
  position: sticky;
  top: 0;
  z-index: 10;

  margin-bottom: 20px;

  background: ${colors.body};

  box-shadow: 0px 2px 12px 0px rgba(0, 0, 0, 0.08);

  @media (max-width: 480px) {
    margin-bottom: 30px;
  }
`;

export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;
  max-width: 960px;
  height: 60px;

  margin: 0 auto;
  padding: 0 8px;

  @media (max-width: 640px) {
    flex-direction: column;
    justify-content: center;

    height: 96px;
  }
`;

export const Title = styled.div`
  display: flex;
  align-items: center;

  font-size: 17px;

  @media (max-width: 640px) {
    margin-bottom: 8px;
  }

  @media (max-width: 380px) {
    font-size: 15px;
  }
`;

export const Logo = styled.img`
  height: 32px;

  margin: 6px 8px 0 0;

  @media (max-width: 380px) {
    height: 28px;
  }
`;

export const Address = styled.a``;

export const Menu = styled.nav``;

export const List = styled.ul`
  list-style: none;
`;

export const Item = styled.li`
  display: inline-block;

  font-size: 16px;

  &:not(:first-child) {
    margin-left: 16px;
  }

  .active {
    color: ${colors.primary};
  }
`;
