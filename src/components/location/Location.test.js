import React from 'react';
import { shallow } from 'enzyme';
import Location from './Location';

describe('tests the Location component', () => {
  const wrapper = shallow(<Location match={{ params: { id: 1 } }} />);

  it('renders the Location component', () => {
    expect(wrapper.find('[data-test="Location"]').exists()).toBe(true);
  });
});
