import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

import Loading from '../shared/loading/Loading';
import PageHeader from '../shared/pageHeader/PageHeader';
import CharacterCard from '../shared/characterCard/CharacterCard';
import {
  LocationStyled,
  Information,
  Name,
  Type,
  Dimension,
  Residents,
  Characters,
} from './Location.styled';

export default function Location(props) {
  return props.match.params.id ? (
    <LocationStyled data-test="Location">
      <Query
        query={gql`
          query Location($id: ID) {
            location(id: $id) {
              name
              type
              dimension
              residents {
                id
                name
                status
                species
                gender
                origin {
                  id
                  name
                }
                image
              }
            }
          }
        `}
        variables={{
          id: props.match.params.id,
        }}
        fetchPolicy="cache-and-network"
        // Buggy API, often returns strings instead of iterables.
        errorPolicy="ignore"
      >
        {({ loading, error, data: { location } }) => {
          if (error) {
            return <p>Error :(</p>;
          }
          return (
            <Fragment>
              <PageHeader title="Location Details" />
              {loading ? (
                <Loading text="Loading location..." />
              ) : (
                <Fragment>
                  {' '}
                  <Information>
                    <Name>{location.name}</Name>
                    <Type>{location.type}</Type>
                    <Dimension>{location.dimension}</Dimension>
                  </Information>
                  <Residents>
                    {location.residents &&
                    Array.isArray(location.residents) &&
                    location.residents.length > 0
                      ? 'Residents of this location:'
                      : 'Has no known residents.'}
                  </Residents>
                  <Characters>
                    {location.residents &&
                      location.residents.map(character => (
                        <CharacterCard
                          key={character.id}
                          character={character}
                        />
                      ))}
                  </Characters>
                </Fragment>
              )}
            </Fragment>
          );
        }}
      </Query>
    </LocationStyled>
  ) : (
    <div>No character selected.</div>
  );
}

Location.propTypes = {
  match: PropTypes.object.isRequired,
};
