import React from 'react';
import { shallow } from 'enzyme';
import Loading from './Loading';

describe('tests the Loading component', () => {
  const wrapper = shallow(<Loading text="Loading test..." />);

  it('renders the Loading component', () => {
    expect(wrapper.containsMatchingElement('Loading test...')).toBeTruthy();
  });
});
