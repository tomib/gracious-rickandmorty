import React from 'react';
import PropTypes from 'prop-types';

import { LoadingStyled, Portal, Text } from './Loading.styled';

import ImagePortal from '../../../assets/portal.png';

function Loading(props) {
  return (
    <LoadingStyled>
      <Portal src={ImagePortal} alt="Rick & Morty" />
      <Text>{props.text}</Text>
    </LoadingStyled>
  );
}

Loading.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Loading;
