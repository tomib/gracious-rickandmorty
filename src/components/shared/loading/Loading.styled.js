import styled, { keyframes } from 'styled-components';

export const LoadingStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  height: 360px;
`;

const PortalAnimation = keyframes`
  0% {
    transform: translateY(0) rotateZ(0);
  }
  50% {
    transform: translateY(-8px) rotateZ(-3deg);
  }
  100% {
    transform: translateY(0) rotateZ(0);
  }
`;

export const Portal = styled.img`
  height: 200px;

  margin-bottom: 8px;

  animation-name: ${PortalAnimation};
  animation-duration: 2.5s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
`;

export const Text = styled.div`
  font-weight: 700;
`;
