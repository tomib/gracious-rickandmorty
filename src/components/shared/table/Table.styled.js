import styled from 'styled-components';
import colors from '../../../constants/colors';

export const Table = styled.div`
  display: table;
  border-collapse: collapse;

  width: 100%;
`;

export const TableRow = styled.div`
  display: table-row;

  border-bottom: 1px solid ${colors.borders};

  transition: 0.2s background ease-in-out;
`;

export const TableCell = styled.div`
  display: table-cell;
  vertical-align: middle;

  width: ${props => props.width || 'auto'};

  padding: 8px 12px;

  text-align: ${props => props.textAlign || 'center'};

  @media (max-width: 480px) {
    display: ${props => (props.mobileHide ? 'none' : 'table-cell')};
  }
`;

export const TableHeader = styled.div`
  display: table-header-group;

  font-weight: 700;
`;

export const TableBody = styled.div`
  display: table-row-group;

  ${TableRow}:hover {
    background: ${colors.secondary};
  }
`;
