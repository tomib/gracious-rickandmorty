import styled from 'styled-components';

export const PageHeaderStyled = styled.div`
  margin-bottom: 30px;
`;

export const Logo = styled.img`
  display: block;

  width: 90%;

  max-width: 640px;

  margin: 0 auto;

  @media (max-width: 380px) {
    margin-bottom: 12px;
  }
`;

export const Title = styled.h1`
  margin-bottom: 30px;

  font-size: 16px;
  text-align: center;
`;
