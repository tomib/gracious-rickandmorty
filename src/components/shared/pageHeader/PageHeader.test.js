import React from 'react';
import { shallow } from 'enzyme';
import PageHeader from './PageHeader';

describe('tests the PageHeader component', () => {
  const wrapper = shallow(<PageHeader title="Test Page Title" />);

  it('renders the PageHeader component', () => {
    expect(wrapper.containsMatchingElement('Test Page Title')).toBeTruthy();
  });
});
