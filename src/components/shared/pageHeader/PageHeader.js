import React from 'react';
import PropTypes from 'prop-types';

import { PageHeaderStyled, Logo, Title } from './PageHeader.styled';

import LogoRM from '../../../assets/rickandmorty.svg';

function PageHeader(props) {
  return (
    <PageHeaderStyled>
      <Logo src={LogoRM} alt="Rick & Morty" />
      <Title>{props.title}</Title>
    </PageHeaderStyled>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PageHeader;
