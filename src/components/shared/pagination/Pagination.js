import React from 'react';
import PropTypes from 'prop-types';

import Button from '../button/Button';
import { PaginationStyled, Page, Controls } from './Pagination.styled';

function Pagination(props) {
  return (
    <PaginationStyled>
      <Page>
        {`Page ${props.currentPage} ${
          props.totalPages ? `of ${props.totalPages}` : ''
        }`}
      </Page>
      <Controls>
        <Button
          type="button"
          disabled={props.prevDisabled}
          onClick={props.onPrevPage}
        >
          Prev
        </Button>
        <Button
          type="button"
          disabled={props.nextDisabled}
          onClick={props.onNextPage}
        >
          Next
        </Button>
      </Controls>
    </PaginationStyled>
  );
}

Pagination.propTypes = {
  currentPage: PropTypes.number,
  totalPages: PropTypes.number,
  prevDisabled: PropTypes.bool,
  nextDisabled: PropTypes.bool,
  onPrevPage: PropTypes.func,
  onNextPage: PropTypes.func,
};

Pagination.defaultProps = {
  currentPage: null,
  totalPages: null,
  prevDisabled: true,
  nextDisabled: true,
  onPrevPage: () => {},
  onNextPage: () => {},
};

export default Pagination;
