import React from 'react';
import { shallow } from 'enzyme';
import Pagination from './Pagination';

describe('tests the Pagination component', () => {
  const wrapper = shallow(<Pagination currentPage={2} totalPages={5} />);

  it('renders the Pagination component', () => {
    expect(wrapper.containsMatchingElement('Page 2 of 5')).toBeTruthy();
    expect(wrapper.containsMatchingElement('Prev')).toBeTruthy();
    expect(wrapper.containsMatchingElement('Next')).toBeTruthy();
  });
});
