import styled from 'styled-components';

import { ButtonStyled } from '../button/Button.styled';

export const PaginationStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  position: sticky;
  bottom: 0;

  height: 46px;

  padding: 0 8px;

  background: rgba(255, 255, 255, 0.98);
`;

export const Page = styled.div``;

export const Controls = styled.div`
  display: flex;
  align-items: center;

  ${ButtonStyled}:not(:last-child) {
    margin-right: 8px;
  }
`;
