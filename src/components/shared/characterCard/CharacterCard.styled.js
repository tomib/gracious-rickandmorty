import styled from 'styled-components';
import colors from '../../../constants/colors';

export const Image = styled.img`
  width: 100%;

  transition: 0.3s opacity ease-in-out;
`;

export const CharacterCardStyled = styled.div`
  display: inline-block;
  vertical-align: top;

  width: 23%;
  height: 360px;

  margin: 4px 1%;

  border-radius: 4px;

  overflow: hidden;

  transition: 0.3s all ease-in-out;

  &:hover {
    transform: translateY(2px);

    ${Image} {
      opacity: 0.9;
    }
  }

  @media (max-width: 720px) {
    width: 31%;
  }

  @media (max-width: 640px) {
    height: 340px;
  }

  @media (max-width: 460px) {
    width: 48%;
  }

  @media (max-width: 330px) {
    height: 300px;
  }
`;

export const Header = styled.div`
  position: relative;
`;

export const Name = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;

  width: 100%;

  padding: 4px 8px;

  background: ${colors.primary}DD;
  color: ${colors.body};

  font-size: 16px;
  font-weight: 700;
`;

export const Information = styled.div`
  padding: 6px 0 8px 0;

  background: ${colors.secondary};

  line-height: 1.5;

  border: 1px solid ${colors.borders};
  border-top: 0;

  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
`;

export const Label = styled.div`
  padding: 0 8px;

  color: ${colors.label};

  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
`;

export const Species = styled.div`
  margin-bottom: 4px;
  padding: 0 8px;

  font-size: 13px;
`;

export const Gender = styled.div`
  margin-bottom: 4px;
  padding: 0 8px;

  font-size: 13px;
`;

export const Origin = styled.div`
  padding: 0 8px;

  font-size: 13px;
`;
