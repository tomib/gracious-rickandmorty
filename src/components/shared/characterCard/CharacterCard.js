import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  CharacterCardStyled,
  Header,
  Image,
  Name,
  Information,
  Label,
  Species,
  Gender,
  Origin,
} from './CharacterCard.styled';

function CharacterCard({ character }) {
  return (
    <CharacterCardStyled>
      <Header>
        <Link to={`/character/${character.id}`}>
          <Image src={character.image} alt="" />
          <Name>{character.name}</Name>
        </Link>
      </Header>
      <Information>
        <Label>Species</Label>
        <Species>{character.species}</Species>
        <Label>Gender</Label>
        <Gender>{character.gender}</Gender>
        <Label>Origin</Label>
        <Origin>{character.origin.name}</Origin>
      </Information>
    </CharacterCardStyled>
  );
}

CharacterCard.propTypes = {
  character: PropTypes.object.isRequired,
};

export default CharacterCard;
