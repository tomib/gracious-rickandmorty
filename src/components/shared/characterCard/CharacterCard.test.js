import React from 'react';
import { shallow } from 'enzyme';
import CharacterCard from './CharacterCard';

describe('tests the CharacterCard component', () => {
  const wrapper = shallow(
    <CharacterCard
      character={{
        id: 3,
        gender: 'Female',
        image: 'https://rickandmortyapi.com/api/character/avatar/3.jpeg',
        name: 'Summer Smith',
        origin: {
          id: '20',
          name: 'Earth (Replacement Dimension)',
          __typename: 'Location',
        },
        species: 'Human',
      }}
    />,
  );

  it('renders the CharacterCard component', () => {
    expect(wrapper.containsMatchingElement('Summer Smith')).toBeTruthy();
    expect(wrapper.containsMatchingElement('Human')).toBeTruthy();
    expect(wrapper.containsMatchingElement('Female')).toBeTruthy();
    expect(
      wrapper.containsMatchingElement('Earth (Replacement Dimension)'),
    ).toBeTruthy();
  });
});
