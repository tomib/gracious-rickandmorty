import styled from 'styled-components';
import colors from '../../../constants/colors';

export const ButtonStyled = styled.button`
  display: flex;
  align-items: center;

  height: 28px;

  padding: 0 12px;

  background: ${colors.body};
  color: ${colors.text};

  line-height: 1;

  border: 1px solid ${colors.borders};
  border-radius: 4px;
  outline: none;

  cursor: pointer;

  transition: 0.3s all ease-in-out;

  &:disabled {
    background: ${colors.secondary};

    opacity: 0.5;

    cursor: not-allowed;
  }

  &:hover:not(:disabled) {
    border: 1px solid ${colors.text};
  }

  &:focus {
    box-shadow: 0px 0px 6px 0px rgba(84, 93, 255, 1);
  }
`;
