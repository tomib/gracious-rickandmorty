import React from 'react';
import PropTypes from 'prop-types';

import { ButtonStyled } from './Button.styled';

function Button(props) {
  return <ButtonStyled {...props}>{props.children}</ButtonStyled>;
}

Button.propTypes = {
  children: PropTypes.node,
};

Button.defaultProps = {
  children: 'A generic button',
};

export default Button;
