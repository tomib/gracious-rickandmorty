import styled from 'styled-components';

export const CharactersStyled = styled.div``;

export const State = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  height: 260px;
`;

export const Cards = styled.div`
  font-size: 0;
`;
