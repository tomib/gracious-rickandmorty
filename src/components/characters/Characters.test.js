import React from 'react';
import { shallow } from 'enzyme';
import Characters from './Characters';

describe('tests the Characters component', () => {
  const wrapper = shallow(
    <Characters history={{ push: () => {} }} match={{ params: { id: 1 } }} />,
  );

  it('renders the Characters component', () => {
    expect(wrapper.find('[data-test="Characters"]').exists()).toBe(true);
  });
});
