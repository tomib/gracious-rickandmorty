import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

import PageHeader from '../shared/pageHeader/PageHeader';
import Loading from '../shared/loading/Loading';
import CharacterCard from '../shared/characterCard/CharacterCard';
import Pagination from '../shared/pagination/Pagination';
import { CharactersStyled, State, Cards } from './Characters.styled';

export default function Characters(props) {
  return (
    <CharactersStyled data-test="Characters">
      <PageHeader title="Character List" />
      <Query
        query={gql`
          query Characters($page: Int) {
            characters(page: $page) {
              info {
                count
                pages
                next
                prev
              }
              results {
                id
                name
                status
                species
                gender
                origin {
                  id
                  name
                }
                image
              }
            }
          }
        `}
        variables={{
          page: Number(props.match.params.page) || 1,
        }}
        fetchPolicy="cache-and-network"
        // Buggy API, often returns strings instead of iterables.
        errorPolicy="ignore"
      >
        {({ loading, error, data: { characters } }) => {
          return (
            <Fragment>
              <Cards>
                {!loading &&
                  characters.results.map(character => (
                    <CharacterCard key={character.id} character={character} />
                  ))}
              </Cards>
              {loading && <Loading text="Loading characters..." />}
              {error && (
                <State>
                  There has been an error. Please try again in a few moments.
                </State>
              )}
              <Pagination
                currentPage={Number(props.match.params.page) || 1}
                totalPages={
                  loading || error ? undefined : Number(characters.info.pages)
                }
                prevDisabled={loading || !characters.info.prev}
                nextDisabled={loading || !characters.info.next}
                onPrevPage={() =>
                  props.history.push(`/characters/${characters.info.prev}`)
                }
                onNextPage={() =>
                  props.history.push(`/characters/${characters.info.next}`)
                }
              />
            </Fragment>
          );
        }}
      </Query>
    </CharactersStyled>
  );
}

Characters.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};
