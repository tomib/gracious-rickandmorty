import styled from 'styled-components';

export const EpisodesStyled = styled.div``;

export const State = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  height: 260px;
`;
