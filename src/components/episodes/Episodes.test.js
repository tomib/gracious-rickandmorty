import React from 'react';
import { shallow } from 'enzyme';
import Episodes from './Episodes';

describe('tests the Episodes component', () => {
  const wrapper = shallow(
    <Episodes history={{ push: () => {} }} match={{ params: { id: 1 } }} />,
  );

  it('renders the Episodes component', () => {
    expect(wrapper.find('[data-test="Episodes"]').exists()).toBe(true);
  });
});
