import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

import PageHeader from '../shared/pageHeader/PageHeader';
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCell,
} from '../shared/table/Table.styled';
import Loading from '../shared/loading/Loading';
import Pagination from '../shared/pagination/Pagination';
import { EpisodesStyled, State } from './Episodes.styled';

export default function Episodes(props) {
  return (
    <EpisodesStyled data-test="Episodes">
      <PageHeader title="Episode List" />
      <Query
        query={gql`
          query Episodes($page: Int) {
            episodes(page: $page) {
              info {
                count
                pages
                next
                prev
              }
              results {
                id
                name
                episode
                air_date
                characters {
                  id
                  name
                }
              }
            }
          }
        `}
        variables={{
          page: Number(props.match.params.page) || 1,
        }}
        fetchPolicy="cache-and-network"
        // Buggy API, often returns strings instead of iterables.
        errorPolicy="ignore"
      >
        {({ loading, error, data: { episodes } }) => {
          return (
            <Fragment>
              <Table>
                <TableHeader>
                  <TableRow>
                    <TableCell width="15%" textAlign="left">
                      Episode
                    </TableCell>
                    <TableCell width="30%" textAlign="left">
                      Name
                    </TableCell>
                    <TableCell>Aired On</TableCell>
                    <TableCell width="15%" mobileHide>
                      Characters
                    </TableCell>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {!loading &&
                    episodes.results.map(
                      ({ id, episode, name, air_date, characters }) => (
                        <TableRow key={episode}>
                          <TableCell width="10%" textAlign="left">
                            {episode}
                          </TableCell>
                          <TableCell width="30%" textAlign="left">
                            <Link to={`/episode/${id}`}>{name}</Link>
                          </TableCell>
                          <TableCell>{air_date}</TableCell>
                          <TableCell width="15%" mobileHide>
                            {characters.length}
                          </TableCell>
                        </TableRow>
                      ),
                    )}
                </TableBody>
              </Table>
              {loading && <Loading text="Loading episodes..." />}
              {error && (
                <State>
                  There has been an error. Please try again in a few moments.
                </State>
              )}
              <Pagination
                currentPage={Number(props.match.params.page) || 1}
                totalPages={
                  loading || error ? undefined : Number(episodes.info.pages)
                }
                prevDisabled={loading || !episodes.info.prev}
                nextDisabled={loading || !episodes.info.next}
                onPrevPage={() =>
                  props.history.push(`/episodes/${episodes.info.prev}`)
                }
                onNextPage={() =>
                  props.history.push(`/episodes/${episodes.info.next}`)
                }
              />
            </Fragment>
          );
        }}
      </Query>
    </EpisodesStyled>
  );
}

Episodes.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};
