import styled from 'styled-components';

export const AppStyled = styled.div`
  height: 100%;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 960px;

  margin: 0 auto;

  @media (max-width: 960px) {
    padding: 0 8px;
  }
`;
