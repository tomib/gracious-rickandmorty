import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import { Scrollbars } from 'react-custom-scrollbars';

import Navigation from '../navigation/Navigation';
import Episodes from '../episodes/Episodes';
import Episode from '../episode/Episode';
import Characters from '../characters/Characters';
import Character from '../character/Character';
import Locations from '../locations/Locations';
import Location from '../location/Location';
import { AppStyled, Content } from './App.styled';

import colors from '../../constants/colors';

function App() {
  return (
    <Scrollbars style={{ height: '100%' }}>
      <AppStyled data-test="App">
        <GlobalStyle />
        <Navigation />
        <Content>
          <Switch>
            <Route exact path="/episodes/:page?" component={Episodes} />
            <Route exact path="/episode/:id" component={Episode} />
            <Route exact path="/characters/:page?" component={Characters} />
            <Route exact path="/character/:id" component={Character} />
            <Route exact path="/locations/:page?" component={Locations} />
            <Route exact path="/location/:id" component={Location} />
            <Redirect to="/episodes" />
          </Switch>
        </Content>
      </AppStyled>
    </Scrollbars>
  );
}

const GlobalStyle = createGlobalStyle`
  html, body, h1, h2, p, ul {
    margin: 0;
    padding: 0;
  }

  html, body {
    height: 100%;
  }

  * {
    box-sizing: border-box;
  }

  body {
    background: ${colors.body};
    color: ${colors.text};

    font-family: 'Open Sans', sans-serif;
    font-size: 14px;

    @media (max-width: 640px) {
      font-size: 12px;
    }
  }

  a {
    color: ${colors.text};

    font-weight: 700;
    text-decoration: none;

    transition: 0.2s color ease-in-out;

    &:hover {
      color: ${colors.primary};
    }
  }

  button, input, select {
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;

    @media (max-width: 640px) {
      font-size: 12px;
    }
  }

  #root {
    height: 100%;
  }
`;

export default App;
