import styled from 'styled-components';
import colors from '../../constants/colors';

export const EpisodeStyled = styled.div``;

export const Information = styled.div`
  margin-bottom: 30px;

  text-align: center;
`;

export const Title = styled.h2`
  font-size: 32px;

  @media (max-width: 460px) {
    font-size: 20px;
  }
`;

export const Number = styled.div`
  margin-bottom: 8px;

  font-size: 22px;

  @media (max-width: 460px) {
    font-size: 16px;
  }
`;

export const Aired = styled.div`
  color: ${colors.primary};
`;

export const Appeared = styled.div`
  margin-bottom: 14px;

  text-align: center;
`;

export const Characters = styled.div`
  font-size: 0;
`;
