import React from 'react';
import { shallow } from 'enzyme';
import Episode from './Episode';

describe('tests the Episode component', () => {
  const wrapper = shallow(<Episode match={{ params: { id: 1 } }} />);

  it('renders the Episode component', () => {
    expect(wrapper.find('[data-test="Episode"]').exists()).toBe(true);
  });
});
