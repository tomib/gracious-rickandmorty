import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

import Loading from '../shared/loading/Loading';
import PageHeader from '../shared/pageHeader/PageHeader';
import CharacterCard from '../shared/characterCard/CharacterCard';
import {
  EpisodeStyled,
  Information,
  Title,
  Number,
  Aired,
  Appeared,
  Characters,
} from './Episode.styled';

export default function Episode(props) {
  return props.match.params.id ? (
    <EpisodeStyled data-test="Episode">
      <Query
        query={gql`
          query Episode($id: ID) {
            episode(id: $id) {
              name
              air_date
              episode
              characters {
                id
                name
                status
                species
                gender
                origin {
                  id
                  name
                }
                image
              }
            }
          }
        `}
        variables={{
          id: props.match.params.id,
        }}
        fetchPolicy="cache-and-network"
        // Buggy API, often returns strings instead of iterables.
        errorPolicy="ignore"
      >
        {({ loading, error, data: { episode } }) => {
          if (error) {
            return <p>Error :(</p>;
          }

          return (
            <Fragment>
              <PageHeader title="Episode Details" />
              {loading ? (
                <Loading text="Loading episode..." />
              ) : (
                <Fragment>
                  {' '}
                  <Information>
                    <Title>{episode.name}</Title>
                    <Number>{episode.episode}</Number>
                    <Aired>Aired on {episode.air_date}</Aired>
                  </Information>
                  <Appeared>These characters appeared in the episode:</Appeared>
                  <Characters>
                    {episode.characters.map(character => (
                      <CharacterCard key={character.id} character={character} />
                    ))}
                  </Characters>
                </Fragment>
              )}
            </Fragment>
          );
        }}
      </Query>
    </EpisodeStyled>
  ) : (
    <div>No episode selected.</div>
  );
}

Episode.propTypes = {
  match: PropTypes.object.isRequired,
};
