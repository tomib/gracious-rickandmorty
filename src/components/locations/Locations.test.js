import React from 'react';
import { shallow } from 'enzyme';
import Locations from './Locations';

describe('tests the Locations component', () => {
  const wrapper = shallow(
    <Locations history={{ push: () => {} }} match={{ params: { id: 1 } }} />,
  );

  it('renders the Locations component', () => {
    expect(wrapper.find('[data-test="Locations"]').exists()).toBe(true);
  });
});
