import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

import PageHeader from '../shared/pageHeader/PageHeader';
import Loading from '../shared/loading/Loading';
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCell,
} from '../shared/table/Table.styled';
import Pagination from '../shared/pagination/Pagination';
import { LocationsStyled, State } from './Locations.styled';

export default function Locations(props) {
  return (
    <LocationsStyled data-test="Locations">
      <PageHeader title="Location List" />
      <Query
        query={gql`
          query Locations($page: Int) {
            locations(page: $page) {
              info {
                count
                pages
                next
                prev
              }
              results {
                id
                name
                type
                dimension
                residents {
                  id
                  name
                }
              }
            }
          }
        `}
        variables={{
          page: Number(props.match.params.page) || 1,
        }}
        fetchPolicy="cache-and-network"
        // Buggy API, often returns strings instead of iterables.
        errorPolicy="ignore"
      >
        {({ loading, error, data: { locations } }) => {
          return (
            <Fragment>
              <Table>
                <TableHeader>
                  <TableRow>
                    <TableCell width="30%" textAlign="left">
                      Name
                    </TableCell>
                    <TableCell width="20%">Type</TableCell>
                    <TableCell width="35%">Dimension</TableCell>
                    <TableCell width="15%" mobileHide>
                      Residents
                    </TableCell>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {!loading &&
                    locations.results.map(
                      ({ id, name, type, dimension, residents }) => (
                        <TableRow key={id}>
                          <TableCell width="30%" textAlign="left">
                            <Link to={`/location/${id}`}>{name}</Link>
                          </TableCell>
                          <TableCell width="20%">{type}</TableCell>
                          <TableCell width="35%">{dimension}</TableCell>
                          <TableCell width="15%" mobileHide>
                            {Array.isArray(residents) ? residents.length : '?'}
                          </TableCell>
                        </TableRow>
                      ),
                    )}
                </TableBody>
              </Table>
              {loading && <Loading text="Loading locations..." />}
              {error && (
                <State>
                  There has been an error. Please try again in a few moments.
                </State>
              )}
              <Pagination
                currentPage={Number(props.match.params.page) || 1}
                totalPages={
                  loading || error ? undefined : Number(locations.info.pages)
                }
                prevDisabled={loading || !locations.info.prev}
                nextDisabled={loading || !locations.info.next}
                onPrevPage={() =>
                  props.history.push(`/locations/${locations.info.prev}`)
                }
                onNextPage={() =>
                  props.history.push(`/locations/${locations.info.next}`)
                }
              />
            </Fragment>
          );
        }}
      </Query>
    </LocationsStyled>
  );
}

Locations.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};
