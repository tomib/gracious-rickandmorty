export default {
  primary: '#22D5A0',
  secondary: '#f1f5f9',
  text: '#1e3064',
  body: '#fff',
  borders: '#eee',
  label: '#b1b1b1',
};
